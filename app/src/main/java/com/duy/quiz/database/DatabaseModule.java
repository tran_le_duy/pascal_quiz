package com.duy.quiz.database;

import android.database.sqlite.SQLiteDatabase;

import com.duy.quiz.QuizApplication;
import com.duy.quiz.logic.UserLogicFactory;
import com.duy.quiz.model.DaoMaster;
import com.duy.quiz.model.DaoSession;

/**
 * Created by funkv on 06.03.2016.
 * <p/>
 * Modules that provides DataSource objects
 */
public class DatabaseModule {
    public static final String DB_NAME = "DB_QUESTION";
    private DaoMaster.DevOpenHelper mDevOpenHelper;
    private SQLiteDatabase mDatabase;
    private DaoMaster daoMaster;
    private QuizApplication application;

    /**
     * Constructs a DatabaseModule and instantiates its context and database name
     *
     * @param context Context
     */
    public DatabaseModule(QuizApplication context) {
        mDevOpenHelper = new DaoMaster.DevOpenHelper(context, DB_NAME, null);
        mDatabase = mDevOpenHelper.getWritableDatabase();
        daoMaster = new DaoMaster(mDatabase);
        this.application = context;
    }

    /**
     * Provides the DaoSession
     *
     * @return DaoSession
     */
    public DaoSession provideSession() {
        return daoMaster.newSession();
    }

    public SQLiteDatabase provideDatabase() {
        return mDevOpenHelper.getWritableDatabase();
    }


    public AnswerDataSource provideAnswerDataSource() {
        return new AnswerDataSource(provideSession());
    }


    public CategoryDataSource provideCategoryDataSource() {
        return new CategoryDataSource(provideSession());
    }


    public ChallengeDataSource provideChallengeDataSource() {
        return new ChallengeDataSource(provideSession());
    }

    public CompletionDataSource provideCompletionDataSource() {
        return new CompletionDataSource(provideSession());
    }


    public StatisticsDataSource provideStatisticsDataSource() {
        return new StatisticsDataSource(provideSession());
    }

    public UserLogicFactory provideUserLogicFactory() {
        return new UserLogicFactory(application, provideSession());
    }

    public UserDataSource provideUserDataSource() {
        return new UserDataSource(provideSession());
    }
}
