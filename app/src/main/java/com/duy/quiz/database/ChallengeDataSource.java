package com.duy.quiz.database;

import com.duy.quiz.model.Challenge;
import com.duy.quiz.model.ChallengeDao;
import com.duy.quiz.model.DaoSession;

import java.util.List;

/**
 * Created by Daniel Hoogen on 25/02/2016.
 * <p/>
 * Data Source class for custom access to challenge table entries in the database
 */
public class ChallengeDataSource {
    //Attributes
    private DaoSession mDaoSession;

    /**
     * Constructor which saves all given parameters to local member attributes.
     *
     * @param session the session to be saved as a member attribute
     */
    public ChallengeDataSource(DaoSession session) {
        mDaoSession = session;
    }

    /**
     * Returns a list with all test_pascal
     *
     * @return list with all test_pascal
     */
    public List<Challenge> getAll() {
        return mDaoSession.getChallengeDao().loadAll();
    }

    public void clear() {
        mDaoSession.clear();
    }

    /**
     * Returns the Challenge object with the given id
     *
     * @param id challenge id in the challenge table
     * @return Challenge object with the given id
     */
    public Challenge getById(long id) {
        return mDaoSession.getChallengeDao().load(id);
    }

    /**
     * Adds a Challenge object to the database
     *
     * @param challenge challenge to be created in the challenge table
     * @return id of the created object
     */
    public long create(Challenge challenge) {
        return mDaoSession.getChallengeDao().insert(challenge);
    }

    /**
     * Returns all test_pascal with the given category id
     *
     * @param categoryId the category id of the category whose test_pascal are returned
     * @return list of test_pascal with the given category id
     */
    public List<Challenge> getByCategoryId(long categoryId) {
        if (categoryId == CategoryDataSource.CATEGORY_ID_ALL) {
            return getAll();
        } else {
            return mDaoSession.getChallengeDao().queryBuilder()
                    .where(ChallengeDao.Properties.CategoryId.eq(categoryId)).list();
        }
    }
}