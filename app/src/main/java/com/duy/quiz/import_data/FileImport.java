package com.duy.quiz.import_data;

import com.duy.quiz.QuizApplication;
import com.duy.quiz.import_data.exceptions.ElementAmountException;
import com.duy.quiz.import_data.exceptions.FileFormatException;
import com.duy.quiz.import_data.exceptions.InvalidAttributeException;
import com.duy.quiz.import_data.exceptions.UnexpectedElementException;
import com.duy.quiz.import_data.xls.XlsxRead;
import com.duy.quiz.import_data.xml.XmlObjects;
import com.duy.quiz.import_data.xml.XmlRead;
import com.duy.quiz.model.Answer;
import com.duy.quiz.model.Category;
import com.duy.quiz.model.Challenge;

import org.w3c.dom.Node;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Daniel Hoogen on 19/02/2016.
 * <p/>
 * Contains the logic for importing files
 */
public class FileImport {
    /**
     * Imports a .bpc file containing categories with test_pascal and their answers into the database
     *
     * @param is          the input stream of the file to be imported
     * @param application the QuizApplication instance
     * @throws FileFormatException        if file is no xml file
     * @throws UnexpectedElementException if an unexpected element was fond in the file
     * @throws ElementAmountException     if an element occurs more or less often than expected
     * @throws InvalidAttributeException  if an attribute has an invalid value
     */
    public static void importXML(InputStream is, QuizApplication application)
            throws FileFormatException, UnexpectedElementException, ElementAmountException,
            InvalidAttributeException {

        //Get root element
        Node categoriesNode = XmlRead.getCategoriesNode(is);

        //Get the root's child nodes
        Node childCategories = categoriesNode.getFirstChild();

        //Create lists for saving the categories, test_pascal and answers
        List<Category> categoryList = new ArrayList<>();
        List<Challenge> challengeList = new ArrayList<>();
        List<Answer> answerList = new ArrayList<>();

        //All categories, test_pascal and answers are read first
        //So if there is any syntax error in the file, nothing will be imported
        long i = 0;
        long nextChallengeId = 0;
        while (childCategories != null) {
            if (childCategories.getNodeType() == Node.ELEMENT_NODE) {
                nextChallengeId = XmlObjects.readCategory(childCategories, i, nextChallengeId,
                        categoryList, challengeList, answerList);
                i++;
            }

            childCategories = childCategories.getNextSibling();
        }
        if (i == 0) throw new ElementAmountException("<category>", ">0", "0");

        //No syntax errors were found, so the read information is being written
        Writer writer = new Writer(application);
        writer.writeAll(categoryList, challengeList, answerList);
    }

    public static void importXlsx(QuizApplication application)
            throws FileFormatException, UnexpectedElementException, ElementAmountException,
            InvalidAttributeException {
        //Create lists for saving the categories, test_pascal and answers
        List<Category> categoryList = new ArrayList<>();
        List<Challenge> challengeList = new ArrayList<>();
        List<Answer> answerList = new ArrayList<>();
        XlsxRead.readExcelFile(application, "excel_xls.xls", categoryList, challengeList, answerList);

        //No syntax errors were found, so the read information is being written
        Writer writer = new Writer(application);
        writer.writeAll(categoryList, challengeList, answerList);
    }

    public static void importXls(QuizApplication application, String name)
            throws FileFormatException, UnexpectedElementException, ElementAmountException,
            InvalidAttributeException {
        //Create lists for saving the categories, test_pascal and answers
        List<Category> categoryList = new ArrayList<>();
        List<Challenge> challengeList = new ArrayList<>();
        List<Answer> answerList = new ArrayList<>();
        XlsxRead.readExcelFile(application, name, categoryList, challengeList, answerList);

        //No syntax errors were found, so the read information is being written
        Writer writer = new Writer(application);
        writer.writeAll(categoryList, challengeList, answerList);
    }
}