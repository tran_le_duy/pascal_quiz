package com.duy.quiz.import_data.xls;

import android.util.Log;

import com.duy.quiz.QuizApplication;
import com.duy.quiz.database.ChallengeType;
import com.duy.quiz.import_data.exceptions.ElementAmountException;
import com.duy.quiz.import_data.exceptions.InvalidAttributeException;
import com.duy.quiz.import_data.exceptions.UnexpectedElementException;
import com.duy.quiz.model.Answer;
import com.duy.quiz.model.Category;
import com.duy.quiz.model.Challenge;

import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.FormulaEvaluator;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Workbook;

import java.io.InputStream;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Duy on 20-Mar-17.
 */

public class XlsxRead {
    public static final String TAG = XlsxRead.class.getSimpleName();
    private final static HashMap<Integer, String> MAP_ANSWER = new HashMap<>();

    static {
        MAP_ANSWER.clear();
        MAP_ANSWER.put(1, "a");
        MAP_ANSWER.put(2, "b");
        MAP_ANSWER.put(3, "c");
        MAP_ANSWER.put(4, "d");
    }

    /**
     * Creates a category object from a category node
     *
     * @param categoryId    the id to be assigned to the read category
     * @param challengeId   the first free challengeId
     * @param categoryList  the list created categories are added to
     * @param challengeList the list created challenges are added to
     * @param answerList    the list created answers are added to
     * @return the next free challengeId
     * @throws UnexpectedElementException if an unexpected element was found in the file
     * @throws ElementAmountException     if an element occurs more or less often than expected
     * @throws InvalidAttributeException  if an attribute has an invalid value
     */
    public static long readCategory(Workbook workbook,
                                    long categoryId,
                                    long challengeId,
                                    List<Category> categoryList,
                                    List<Challenge> challengeList,
                                    List<Answer> answerList)
            throws UnexpectedElementException, ElementAmountException, InvalidAttributeException {


        // Get the first sheet from workbook
        HSSFSheet sheet = (HSSFSheet) workbook.getSheetAt(0);
        FormulaEvaluator formulaEvaluator = workbook.getCreationHelper().createFormulaEvaluator();

        int rowsCount = sheet.getPhysicalNumberOfRows();

        Row imgRow = sheet.getRow(0); //image
        Row titleRow = sheet.getRow(1); //title
        Row descRow = sheet.getRow(2);  //desc
        String title = titleRow.getCell(1).toString();
        String description = descRow.getCell(1).toString();
        String image = "@" + imgRow.getCell(1).toString();

        //Add category to the list
        categoryList.add(new Category(categoryId, title, description, image));

        //ignore one row title, start index is 4
        for (int index = 4; index < rowsCount; index++) {
            try {
                HSSFRow row = sheet.getRow(index);

                String question = row.getCell(0).toString();
                //Add challenge to the list
                challengeList.add(new Challenge(challengeId, ChallengeType.MULTIPLE_CHOICE, question, categoryId));
                String correct = row.getCell(5).toString();
                readAnswer(row, challengeId, answerList, correct);
                challengeId++;
            } catch (Exception ignored) {
            }
        }
        return challengeId;
    }

    private static void readAnswer(HSSFRow row, long challengeId, List<Answer> answerList, String correct) {
        for (int i = 1; i <= 4; i++) {
            Cell cell = row.getCell(i);
            String text = cell.toString();
            boolean answerCorrect;
            if (MAP_ANSWER.get(i).equalsIgnoreCase(correct.trim())) {
                answerCorrect = true;
            } else {
                answerCorrect = false;
            }
            //Add answer to the list
            answerList.add(new Answer(null, text, answerCorrect, challengeId));
        }
    }

    /**
     * one row is question and correct answer, | question  | A   | B   |  C |  D  | answer |
     *
     * @param nameFile name of file excel in asset, not include path
     */
    public static void readExcelFile(QuizApplication application, String nameFile,
                                     List<Category> categoryList,
                                     List<Challenge> challengeList, List<Answer> answerList) {
        Log.d(TAG, "readExcelFile: ");
        try {
            // Creating Input Stream
            InputStream stream = application.getAssets().open("question/" + nameFile);
            // Create a workbook using the File System
            HSSFWorkbook workbook = new HSSFWorkbook(stream);
            readCategory(workbook, 1, 1, categoryList, challengeList, answerList);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return;
    }
}
