package com.duy.quiz;

import android.app.Application;
import android.content.Context;

import com.duy.quiz.activities.playchallenge.AnswerFragmentFactory;
import com.duy.quiz.database.CompletionDataSource;
import com.duy.quiz.database.UserDataSource;
import com.duy.quiz.logic.CompletionLogic;
import com.duy.quiz.logic.UserManager;
import com.duy.quiz.logic.statistics.ChartSettings;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by funkv on 06.03.2016.
 *
 * Defines how instances are created for the App
 */
@Module
public class AppModule {
    QuizApplication mApplication;

    public AppModule(QuizApplication application) {
        mApplication = application;
    }

    @Provides
    @Singleton
    Application providesApplication() {
        return mApplication;
    }

    @Provides
    @Singleton
    QuizApplication providesBpApp() {
        return mApplication;
    }

    @Provides
    @Singleton
    UserManager providesUserManager(Application application, UserDataSource userDataSource) {
        return new UserManager(application, userDataSource);
    }

    @Provides
    @Singleton
    Context providesContext() {
        return mApplication.getApplicationContext();
    }

    @Provides
    @Singleton
    ChartSettings providesChartSettings(QuizApplication app) {
        return new ChartSettings(app);
    }

    @Provides
    @Singleton
    AnswerFragmentFactory providesFragmentFactory() { return new AnswerFragmentFactory(); }

    @Provides
    @Singleton
    CompletionLogic providesCompletionLogic(CompletionDataSource ds) {
        return new CompletionLogic(ds);
    }
}
