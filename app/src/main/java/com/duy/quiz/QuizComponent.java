package com.duy.quiz;


import com.duy.quiz.activities.createuser.CreateUserActivity;
import com.duy.quiz.activities.select_category.MainActivity;
import com.duy.quiz.activities.main.SplashActivity;
import com.duy.quiz.activities.playchallenge.AnswerFragment;
import com.duy.quiz.activities.playchallenge.multiplechoice.ButtonViewState;
import com.duy.quiz.activities.playchallenge.multiplechoice.MultipleChoiceFragment;
import com.duy.quiz.activities.playchallenge.selfcheck.SelfTestFragment;
import com.duy.quiz.activities.playchallenge.text.TextFragment;
import com.duy.quiz.activities.select_category.FragmentSelectCategory;
import com.duy.quiz.activities.selectuser.UserAdapter;
import com.duy.quiz.activities.selectuser.UserSelectionActivity;
import com.duy.quiz.activities.statistics.StatisticsActivity;
import com.duy.quiz.import_data.Writer;
import com.duy.quiz.logic.UserLogicFactory;

/**
 * Created by funkv on 06.03.2016.
 * <p>
 * App Component that defines injection targets for DI.
 */
public interface QuizComponent {
    void inject(MainActivity mainActivity);

    void inject(SplashActivity activity);

    void inject(com.duy.quiz.activities.playchallenge.ChallengeActivity challengeActivity);

    void inject(MultipleChoiceFragment questionFragment);

    void inject(TextFragment textFragment);

    void inject(SelfTestFragment selfTestFragment);

    void inject(CreateUserActivity createUserActivity);

    void inject(UserAdapter userAdapter);

    void inject(UserSelectionActivity activity);

    void inject(StatisticsActivity activity);

    void inject(FragmentSelectCategory fragmentSelectCategoryPage);

    void inject(AnswerFragment answerFragment);

    void inject(Writer writer);

    void inject(ButtonViewState state);

    void inject(UserLogicFactory f);

    void inject(SplashActivity.LoadDataTask loadDataTask);
}
