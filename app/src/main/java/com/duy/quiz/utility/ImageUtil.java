package com.duy.quiz.utility;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.SparseArray;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.RequestCreator;

import java.io.File;

/**
 * Created by funkv on 17.02.2016.
 *
 * Gives a combined interface for loading both filesystem images and resource drawables
 * via picasso. Filepaths that are drawable should be prefixed with @drawable/, the filepath is
 * then the resource name.
 */
public class ImageUtil {
    private static SparseArray<RequestCreator> requestCache = new SparseArray<>();

    /**
     * Returns whether the path represents a drawable resource.
     * @param imagePath the path to analyze
     * @return true if the path represents a drawable resource
     */
    public static boolean isDrawableImage(String imagePath) {
        return imagePath.startsWith("@drawable/");
    }

    /**
     * Gets the resource id of an abstract path.
     * @param imagePath imagePath the path to analyze
     * @param context context to use
     * @return resource id of the corresponding drawable
     */
    public static int getResourceId(String imagePath, Context context) {
        if (!isDrawableImage(imagePath)) {
            throw new IllegalArgumentException("Drawable is not a resource drawable.");
        }

        Resources resources = context.getResources();
        String resourceName = imagePath.substring("@drawable/".length());
        return resources.getIdentifier(resourceName, "drawable", context.getPackageName());
    }

    /**
     * Loads an image using Picasso while caching it.
     * @param imagePath path as described above
     * @param context context to use
     * @return cached or newly created RequestCreator
     */
    public static RequestCreator loadImage(String imagePath, Context context) {
        if (!isDrawableImage(imagePath)) {
            return Picasso.with(context).load(new File(imagePath));
        } else {
            int resourceId = getResourceId(imagePath, context);
            RequestCreator requestCreator = requestCache.get(resourceId);
            if (requestCreator == null) {
                requestCreator = Picasso.with(context).load(resourceId);
                requestCache.put(resourceId, requestCreator);
            }

            return requestCreator;
        }
    }

    public static int calculateInSampleSize(BitmapFactory.Options options,
                                            int reqWidth, int reqHeight) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            // Calculate ratios of height and width to requested height and
            // width
            final int heightRatio = Math.round((float) height
                    / (float) reqHeight);
            final int widthRatio = Math.round((float) width / (float) reqWidth);

            // Choose the smallest ratio as inSampleSize value, this will
            // guarantee
            // a final image with both dimensions larger than or equal to the
            // requested height and width.
            inSampleSize = heightRatio < widthRatio ? heightRatio : widthRatio;
        }

        return inSampleSize;
    }

    public static Bitmap decodeSampledBitmapFromResource(Resources res,
                                                         int resId, int reqWidth, int reqHeight) {

        // First decode with inJustDecodeBounds=true to check dimensions
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeResource(res, resId, options);

        // Calculate inSampleSize
        options.inSampleSize = calculateInSampleSize(options, reqWidth,
                reqHeight);

        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;
        return BitmapFactory.decodeResource(res, resId, options);
    }
}
