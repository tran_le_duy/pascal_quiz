package com.duy.quiz;

import android.content.res.Resources;
import android.support.multidex.MultiDexApplication;
import android.util.DisplayMetrics;

import com.duy.quiz.database.DatabaseModule;

import net.danlew.android.joda.JodaTimeAndroid;

/**
 * Created by funkv on 17.02.2016.
 * <p>
 * Custom Application class for hooking into App creation
 */
public class QuizApplication extends MultiDexApplication {
    public static QuizComponent component;
    private DatabaseModule databaseModule;


    /**
     * Returns the Component for use with Dependency Injection for this
     * Application.
     *
     * @return compoenent to use for DI
     */
    public QuizComponent getComponent() {
        return component;
    }

    /**
     * initializes the DaoManager with a writeable database
     */
    @Override
    public void onCreate() {
        super.onCreate();
        JodaTimeAndroid.init(this);
        databaseModule = new DatabaseModule(this);
    }

    /**
     * Licensed under CC BY-SA (c) 2012 Muhammad Nabeel Arif
     * http://stackoverflow.com/questions/4605527/converting-pixels-to-dp
     * <p>
     * This method converts device specific pixels to density independent pixels.
     *
     * @param px A value in px (pixels) unit. Which we need to convert into dp
     * @return A float value to represent dp equivalent to px value
     */
    public float convertPixelsToDp(float px) {
        Resources resources = getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        float dp = px / ((float) metrics.densityDpi / DisplayMetrics.DENSITY_DEFAULT);
        return dp;
    }

    public DatabaseModule getDatabaseModule() {
        return databaseModule;
    }

}