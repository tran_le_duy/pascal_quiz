package com.duy.quiz.activities.selectuser;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;

import com.duy.quiz.QuizApplication;
import com.duy.quiz.QuizComponent;
import com.duy.quiz.R;
import com.duy.quiz.activities.QuizActivity;
import com.duy.quiz.activities.createuser.CreateUserActivity;
import com.duy.quiz.activities.select_category.MainActivity;
import com.duy.quiz.database.DatabaseModule;
import com.duy.quiz.database.UserDataSource;
import com.duy.quiz.logic.UserManager;
import com.duy.quiz.model.Completion;
import com.duy.quiz.model.Statistics;
import com.duy.quiz.model.User;
import com.duy.quiz.utility.DividerItemDecoration;

import java.util.List;

/**
 * Created by Christian Kost
 * <p/>
 * Activity used to chose an existing user. Can load the create user activity and the category selection
 */
public class UserSelectionActivity extends QuizActivity implements UserAdapter.ResultListener {
    private static final String TAG = UserSelectionActivity.class.getSimpleName();
    private UserAdapter mListAdapter;
    private UserManager mUserManager;
    private UserDataSource mUserDataSource;

    /**
     * {@inheritDoc}
     */
    @Override
    protected void injectComponent(QuizComponent component) {
        QuizApplication application = (QuizApplication) getApplication();
        DatabaseModule databaseModule = application.getDatabaseModule();
        mUserDataSource = databaseModule.provideUserDataSource();
        mUserManager = new UserManager(application, mUserDataSource);
        mUserManager.logInLastUser();
    }

    /**
     * Setup the view, adds listener and loads all users
     *
     * @param savedInstanceState Ignored
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_selection);

        Toolbar myChildToolbar =
                (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(myChildToolbar);

        // Get a support ActionBar corresponding to this toolbar
        ActionBar ab = getSupportActionBar();

        // Enable the Up button
        ab.setDisplayHomeAsUpEnabled(true);

        //loading of the components
        RecyclerView userRecycleView = (RecyclerView) findViewById(R.id.userList);
        userRecycleView.setHasFixedSize(true);
        userRecycleView.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL_LIST));
        registerForContextMenu(userRecycleView);

        //load the users from the database
        List<User> allUsers = mUserDataSource.getAll();
        Log.d(TAG, "onCreate: " + allUsers.toString());

        // use a linear layout manager
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        userRecycleView.setLayoutManager(layoutManager);

        //Create the View
        //Adapter which sets all users into the list
        mListAdapter = new UserAdapter(allUsers, this, (QuizApplication) getApplication(), mUserManager);
        userRecycleView.setAdapter(mListAdapter);

        /**
         * Is the add button selected load Create-User-Activity.
         */
        FloatingActionButton btnAddUser = (FloatingActionButton) findViewById(R.id.addUser);
        btnAddUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivityForResult(new Intent(Intent.ACTION_INSERT, Uri.EMPTY, getApplicationContext(), CreateUserActivity.class), 0);
            }
        });
    }

    /**
     * Used to track when the create user activity has finished. If it was cancelled, do nothing,
     * else finish this activity.
     *
     * @param requestCode Ignored
     * @param resultCode  The result of the activity
     * @param data        Ignored
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            setResult(Activity.RESULT_OK);
            finish();
        }
    }

    /**
     * When a user has been selected, finish this activity and load select category activity.
     *
     * @param user To switched user
     */
    @Override
    public void onUserSelected(User user) {
        mUserManager.switchUser(user);

        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);

        setResult(Activity.RESULT_OK);
        finish();
    }

    /**
     * On edit a user, finish this activity and load edit user activity
     *
     * @param user To edited user
     */
    @Override
    public void onEditUser(User user) {
        Intent intent = new Intent(Intent.ACTION_EDIT, Uri.EMPTY, getApplicationContext(), CreateUserActivity.class);
        intent.putExtra(CreateUserActivity.KEY_USER_ID, user.getId());
        startActivityForResult(intent, 0);
    }

    /**
     * On delete a user, remove its data and remove the user from the list
     *
     * @param user     to deleted user
     * @param position position of the user in the list
     */
    @Override
    public void onDeleteUser(User user, int position) {
        //Delete Completions
        List<Completion> completions = user.getCompletions();
        for (Completion completion : completions)
            completion.delete();

        //Delete Statistics
        List<Statistics> statistics = user.getStatistics();
        for (Statistics statistic : statistics)
            statistic.delete();

        //Delete User
        user.delete();

        mListAdapter.removeAt(position);
    }
}