package com.duy.quiz.activities.main;

import android.content.Intent;
import android.content.res.Resources;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.util.Log;

import com.duy.quiz.QuizApplication;
import com.duy.quiz.QuizComponent;
import com.duy.quiz.R;
import com.duy.quiz.activities.QuizActivity;
import com.duy.quiz.activities.createuser.CreateUserActivity;
import com.duy.quiz.activities.select_category.MainActivity;
import com.duy.quiz.database.DatabaseModule;
import com.duy.quiz.database.UserDataSource;
import com.duy.quiz.import_data.FileImport;
import com.duy.quiz.logic.UserManager;
import com.duy.quiz.model.DaoSession;

import java.io.InputStream;
import java.util.ArrayList;

/**
 * Created by funkv on 29.02.2016.
 * <p>
 * The activity redirects to user creation on first launch. On later launches it loads last selected
 * user and redirects to the main activity.
 */
public class SplashActivity extends QuizActivity {
    public static final String TAG = SplashActivity.class.getSimpleName();
    UserManager mUserManager;

    private Handler handler = new Handler();

    @Override
    protected void injectComponent(QuizComponent component) {
        QuizApplication application = (QuizApplication) getApplication();
        DatabaseModule databaseModule = application.getDatabaseModule();
        DaoSession daoSession = databaseModule.provideSession();
        mUserManager = new UserManager(application, new UserDataSource(daoSession));
    }

    /**
     * This method is called when the activity is created
     *
     * @param savedInstanceState handed over to super constructor
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_proxy);
        parseData();
    }


    void startMainActivity() {
        if (mUserManager.logInLastUser()) {
            final Intent intent = new Intent(getApplicationContext(), MainActivity.class);
            intent.putExtra(MainActivity.EXTRA_SHOW_LOGGEDIN_SNACKBAR, true);
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    startActivity(intent);
                }
            }, 500);
        } else {
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    startActivity(new Intent(Intent.ACTION_INSERT, Uri.EMPTY,
                            getApplicationContext(), CreateUserActivity.class));
                }
            }, 500);
        }
    }

    ;

    public void parseData() {
        Log.d(TAG, "parseData: ");
        new LoadDataTask().execute();
    }

    public class LoadDataTask extends AsyncTask<Void, Integer, Void> {
        ArrayList<InputStream> streams;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            Resources resources = getResources();
            streams = new ArrayList<>();
            streams.add(resources.openRawResource(R.raw.test_pascal));
            streams.add(resources.openRawResource(R.raw.test_pascal_1));
            streams.add(resources.openRawResource(R.raw.test_pascal_2));
//            progressBar.setMax(streams.size() + 3);
        }

        @Override
        protected Void doInBackground(Void... params) {
            QuizApplication application = (QuizApplication) getApplication();
//            try {
//                FileImport.importXls(application, "excel_xls2.xls");
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
            // Import test_pascal if the database does not include any
            Boolean imported = PreferenceManager.getDefaultSharedPreferences(SplashActivity.this)
                    .getBoolean("imported_data", false);
            if (!imported) {
                for (int i = 0; i < streams.size(); i++) {
                    InputStream is = streams.get(i);
                    try {
                        FileImport.importXML(is, application);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    publishProgress(i);
                }
                try {
                    FileImport.importXlsx(application);
                    publishProgress(streams.size() + 1);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    FileImport.importXls(application, "excel_xls3.xls");
                    publishProgress(streams.size() + 1);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    FileImport.importXls(application, "excel_xls3.xls");
                    publishProgress(streams.size() + 1);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    FileImport.importXls(application, "excel_xls3.xls");
                    publishProgress(streams.size() + 1);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    FileImport.importXls(application, "excel_xls3.xls");
                    publishProgress(streams.size() + 1);
                } catch (Exception e) {
                    e.printStackTrace();
                }


                PreferenceManager.getDefaultSharedPreferences(SplashActivity.this)
                        .edit().putBoolean("imported_data", true).apply();
            }
            publishProgress(streams.size() + 3);
            return null;
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            super.onProgressUpdate(values);
//            progressBar.setProgress(values[0]);
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            startMainActivity();
        }
    }
}
