package com.duy.quiz.activities.statistics;

import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;

import com.duy.quiz.QuizApplication;
import com.duy.quiz.QuizComponent;
import com.duy.quiz.R;
import com.duy.quiz.activities.QuizActivity;
import com.duy.quiz.activities.playchallenge.ChallengeActivity;
import com.duy.quiz.database.CategoryDataSource;
import com.duy.quiz.database.ChallengeDataSource;
import com.duy.quiz.database.CompletionDataSource;
import com.duy.quiz.database.DatabaseModule;
import com.duy.quiz.database.StatisticsDataSource;
import com.duy.quiz.logic.UserLogicFactory;
import com.duy.quiz.logic.UserManager;
import com.duy.quiz.logic.statistics.ChartDataLogic;
import com.duy.quiz.logic.statistics.StatisticType;
import com.duy.quiz.model.User;

import java.util.ArrayList;
import java.util.List;

import static com.duy.quiz.logic.statistics.StatisticType.TYPE_MOST_PLAYED;

/**
 * Created by Daniel Hoogen on 09/03/2016.
 * <p/>
 * This is the activity showing statistics for the user in a specified category
 */
public class StatisticsActivity extends QuizActivity {
    private static final StatisticType[] TYPES = new StatisticType[]{
            StatisticType.TYPE_ALL_QUESTION,
            TYPE_MOST_PLAYED,
            StatisticType.TYPE_MOST_FAILED,
            StatisticType.TYPE_MOST_SUCCEEDED

    };

    private UserManager mUserManager;
    private UserLogicFactory mUserLogicFactory;
    private ChallengeDataSource mChallengeDataSource;
    private QuizApplication mApplication;
    private CompletionDataSource mCompletionDataSource;
    private StatisticsDataSource mStatisticsDataSource;

    private StatisticsAdapter mAdapter;
    private List<StatisticType> mShownTypes;
    private long mCategoryId;


    @Override
    protected void injectComponent(QuizComponent component) {
        mApplication = (QuizApplication) getApplication();
        DatabaseModule databaseModule = mApplication.getDatabaseModule();
        mUserLogicFactory = databaseModule.provideUserLogicFactory();
        mChallengeDataSource = databaseModule.provideChallengeDataSource();
        mCompletionDataSource = databaseModule.provideCompletionDataSource();
        mStatisticsDataSource = databaseModule.provideStatisticsDataSource();
        mUserManager = new UserManager(mApplication, databaseModule.provideUserDataSource());
        mUserManager.logInLastUser();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_statistics);

        mShownTypes = new ArrayList<>();

        //Add toolbar
        Toolbar myChildToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(myChildToolbar);

        // Get a support ActionBar corresponding to this toolbar
        ActionBar ab = getSupportActionBar();

        // Enable the Up button
        if (ab != null) ab.setDisplayHomeAsUpEnabled(true);

        User user = mUserManager.getCurrentUser();
        mCategoryId = getIntent().getLongExtra(ChallengeActivity.EXTRA_CATEGORY_ID,
                CategoryDataSource.CATEGORY_ID_ALL);

        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.statisticsRecycler);
        recyclerView.setHasFixedSize(true);

        //Look up which statistics can be shown
        setShownTypes();

        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);

        //Add adapter
        mAdapter = new StatisticsAdapter(mUserLogicFactory, mChallengeDataSource,
                (QuizApplication) getApplication(), user, mCategoryId, mShownTypes);

        recyclerView.setAdapter(mAdapter);
    }

    /**
     * This method looks up which charts can be created and sets the member shown types to an array
     * of the types of these charts
     */
    private void setShownTypes() {
        ChartDataLogic chartDataLogic = new ChartDataLogic(
                mUserManager.getCurrentUser(),
                mCategoryId,
                mApplication,
                mChallengeDataSource,
                mCompletionDataSource,
                mStatisticsDataSource,
                mUserLogicFactory);

        mShownTypes.clear();

        for (StatisticType type : TYPES) {
            switch (type) {
                case TYPE_MOST_PLAYED:
                case TYPE_MOST_SUCCEEDED:
                case TYPE_MOST_FAILED:
                    if (chartDataLogic.findMostPlayedData(type, new ArrayList<Long>()) != null) {
                        mShownTypes.add(type);
                    }
                    break;
                case TYPE_ALL_QUESTION:
                    if (chartDataLogic.getAllStatistics().size() > 0)
                        mShownTypes.add(type);
                    break;
            }
        }
    }

    /**
     * This method is called when the activity is started. It resets the shown item views.
     */
    @Override
    protected void onStart() {
        super.onStart();

        setShownTypes();
        mAdapter.setStatisticItems(mShownTypes);
        mAdapter.notifyDataSetChanged();
    }
}