package com.duy.quiz.activities.statistics;

import android.content.Context;
import android.support.v7.widget.LinearLayoutCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.duy.quiz.QuizApplication;
import com.duy.quiz.R;
import com.duy.quiz.database.ChallengeDataSource;
import com.duy.quiz.logic.UserLogicFactory;
import com.duy.quiz.logic.statistics.StatisticType;
import com.duy.quiz.model.User;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Daniel Hoogen on 09/03/2016.
 * <p/>
 * This adapter adds the StatisticViewHolder objects to the recycler view it is assigned to
 */
public class StatisticsAdapter extends RecyclerView.Adapter<StatisticViewHolder> {
    public static final HashMap<StatisticType, Integer> VIEW_TYPE_MAP = new HashMap<>();

    static {
        VIEW_TYPE_MAP.put(StatisticType.TYPE_MOST_PLAYED, StatisticViewHolder.TYPE_LARGE);
        VIEW_TYPE_MAP.put(StatisticType.TYPE_MOST_FAILED, StatisticViewHolder.TYPE_LARGE);
        VIEW_TYPE_MAP.put(StatisticType.TYPE_MOST_SUCCEEDED, StatisticViewHolder.TYPE_LARGE);
        VIEW_TYPE_MAP.put(StatisticType.TYPE_ALL_QUESTION, StatisticViewHolder.TYPE_VALUE);
    }

    //Attributes
    private UserLogicFactory mUserLogicFactory;
    private ChallengeDataSource mChallengeDataSource;
    private QuizApplication mApplication;
    private User mUser;
    private long mCategoryId;
    private List<StatisticType> mStatisticItems;

    /**
     * This constructor saves the given parameters as member attributes and sets the value of the
     * view number.
     *
     * @param userLogicFactory    the user logic factory to be saved as a member attribute
     * @param challengeDataSource the challenge data source to be saved as a member attribute
     * @param application         the QuizApplication to be saved as a member attribute
     * @param user                the user to be saved as a member attribute
     * @param categoryId          the category id to be saved as a member attribute
     */
    public StatisticsAdapter(UserLogicFactory userLogicFactory,
                             ChallengeDataSource challengeDataSource,
                             QuizApplication application, User user, long categoryId,
                             List<StatisticType> itemsToShow) {
        mUserLogicFactory = userLogicFactory;
        mChallengeDataSource = challengeDataSource;
        mApplication = application;
        mUser = user;
        mCategoryId = categoryId;

        mStatisticItems = new ArrayList<>();
        setStatisticItems(itemsToShow);
    }

    /**
     * Called to create the ViewHolder at the given position.
     *
     * @param parent   parent to assign the newly created view to
     * @param viewType ignored
     */
    @Override
    public StatisticViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v;
        int layout;

        Context parentContext = parent.getContext();

        if (viewType == StatisticViewHolder.TYPE_LARGE) {
            layout = R.layout.list_item_statistic_most_played;

        } else if (viewType == StatisticViewHolder.TYPE_VALUE) {
            layout = R.layout.item_statistic_all_question;
        } else {
            throw new RuntimeException("Invalid view type!");
        }

        v = LayoutInflater.from(parentContext).inflate(layout, parent, false);

        LinearLayoutCompat.LayoutParams layoutParams = new LinearLayoutCompat.LayoutParams(
                LinearLayoutCompat.LayoutParams.MATCH_PARENT,
                LinearLayoutCompat.LayoutParams.WRAP_CONTENT);

        v.setLayoutParams(layoutParams);

        return new StatisticViewHolder(v, mUserLogicFactory, mChallengeDataSource, mApplication,
                mUser, mCategoryId);
    }

    /**
     * Returns the type of the item view at the given position
     *
     * @param position the position of the item view
     * @return the type of the item view
     */
    @Override
    public int getItemViewType(int position) {
        StatisticType type = mStatisticItems.get(position);
        return VIEW_TYPE_MAP.get(type);
    }

    /**
     * Called to bind the ViewHolder at the given position.
     *
     * @param holder   the ViewHolder object to be bound
     * @param position the position of the ViewHolder
     */
    @Override
    public void onBindViewHolder(StatisticViewHolder holder, int position) {
        switch (getItemViewType(position)) {
            case StatisticViewHolder.TYPE_LARGE:
                holder.applyMostPlayedChart(mStatisticItems.get(position));
                break;
            case StatisticViewHolder.TYPE_VALUE:
                holder.applyAllQuestionData(mStatisticItems.get(position));
                break;

        }
    }

    /**
     * Returns the count of ViewHolders in the adapter
     *
     * @return the count of ViewHolders
     */
    @Override
    public int getItemCount() {
        return mStatisticItems.size();
    }

    /**
     * Sets the statistic items member to the parameter object
     *
     * @param statisticItems the statistic items to be shown in the recycler view
     */
    public void setStatisticItems(List<StatisticType> statisticItems) {
        mStatisticItems.clear();
        mStatisticItems.addAll(statisticItems);
    }
}