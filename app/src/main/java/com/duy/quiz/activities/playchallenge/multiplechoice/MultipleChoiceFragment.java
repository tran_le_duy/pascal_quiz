package com.duy.quiz.activities.playchallenge.multiplechoice;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.duy.quiz.QuizApplication;
import com.duy.quiz.QuizComponent;
import com.duy.quiz.R;
import com.duy.quiz.activities.playchallenge.AnswerFragment;
import com.duy.quiz.database.DatabaseModule;

import java.util.Arrays;
import java.util.Collections;

import im.delight.android.audio.MusicManager;


/**
 * Created by Christian Kost
 * <p/>
 * Fragment for a multiple-choice challenge
 */
public class MultipleChoiceFragment extends AnswerFragment {
    private static final String KEY_BUTTONS_STATE = "KEY_BUTTONS_STATE";
    private static final String KEY_ANSWERS_CHECKED = "KEY_ANSWERS_CHECKED";
    private RecyclerView mRecyclerView;
    private ButtonsAdapter mAdapter;
    private LinearLayoutManager mLayoutManager;
    // State
    private boolean mAnswersChecked = false;
    private ButtonViewState[] mStateHolder;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    protected void injectComponent(QuizComponent component) {
        QuizApplication application = (QuizApplication) getActivity().getApplication();
        DatabaseModule databaseModule = application.getDatabaseModule();
        mChallengeDataSource = databaseModule.provideChallengeDataSource();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        //inflate the view
        View view = inflater.inflate(R.layout.fragment_challenge_multiple_choice, container, false);
        mRecyclerView = (RecyclerView) view.findViewById(R.id.buttonsRecyclerView);
        mRecyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(getContext());
        mRecyclerView.setLayoutManager(mLayoutManager);
//        mRecyclerView.addItemDecoration(new
//                DividerItemDecoration(getActivity(),
//                DividerItemDecoration.VERTICAL_LIST));

        if (savedInstanceState != null) {
            mStateHolder = (ButtonViewState[]) savedInstanceState.getParcelableArray(KEY_BUTTONS_STATE);
            mAnswersChecked = savedInstanceState.getBoolean(KEY_ANSWERS_CHECKED);
        } else {
            mStateHolder = new ButtonViewState[mAnswerList.size()];
            Collections.shuffle(mAnswerList);
            for (int i = 0; i < mAnswerList.size(); i++) {
                mStateHolder[i] = new ButtonViewState(mAnswerList.get(i));
            }
        }

        mAdapter = new ButtonsAdapter(Arrays.asList(mStateHolder), getActivity());
        mRecyclerView.setAdapter(mAdapter);
        if (mAnswersChecked) {
            mRecyclerView.post(new Runnable() {
                @Override
                public void run() {
                    mAdapter.performAnswerCheck();
                }
            });
        }

        return view;
    }

    /**
     * Checks if the given answer is correct, displays it and passes the event on to the activity
     */
    @Override
    public AnswerFragment.ContinueMode goToNextState() {
        boolean result = mAdapter.performAnswerCheck();

        if (!result) {
            MusicManager.getInstance().play(getActivity(), R.raw.wrong_audio);
        } else {
            MusicManager.getInstance().play(getActivity(), R.raw.correct_audio);
        }

        mListener.onAnswerChecked(result, false);
        mAnswersChecked = true;
        return ContinueMode.CONTINUE_SHOW_FAB;
    }

    /**
     * Saves the current state of the view
     *
     * @param outState Bundle that contains the Challenge-Id and the state of the fragment
     */
    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelableArray(KEY_BUTTONS_STATE, mStateHolder);
        outState.putBoolean(KEY_ANSWERS_CHECKED, mAnswersChecked);
    }
}
