package com.duy.quiz.activities.playchallenge.multiplechoice;

import android.content.Context;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.ColorDrawable;
import android.os.Handler;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.RadioButton;

import com.duy.quiz.R;
import com.duy.quiz.model.Answer;

import java.util.ArrayList;
import java.util.List;

/**
 * Created Christian Kost
 * </p>
 * Adapter which holds the Buttons for a multiple choice challenge
 */
public class ButtonsAdapter extends RecyclerView.Adapter<ButtonsAdapter.ButtonViewHolder> implements ButtonChoiceListener {
    private List<ButtonViewState> mAnswers;
    private List<ButtonViewHolder> mButtons = new ArrayList<>();
    private Context context;

    public ButtonsAdapter(List<ButtonViewState> answers, Context context) {
        mAnswers = answers;
        this.context = context;
        mButtons = new ArrayList<>(mAnswers.size());
    }

    @Override
    public ButtonViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_multiplechoice_button, parent, false);
        return new ButtonViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ButtonViewHolder holder, int position) {
        mButtons.add(position, holder);
        holder.bind(mAnswers.get(position));
        holder.setButtonChoiceListener(this);
    }

    @Override
    public int getItemCount() {
        return mAnswers.size();
    }

    /**
     * Updates the view to display the correct answer and validates user selection.
     *
     * @return true, if the user answered correctly, else false
     */
    public boolean performAnswerCheck() {
        boolean answerCorrect = true;
        for (ButtonViewHolder vh : mButtons) {
            if (!vh.performAnswerCheck()) {
                answerCorrect = false;
            }
        }
        return answerCorrect;
    }

    /**
     * single check, un check other
     *
     * @param pos - pos checked
     */
    @Override
    public void onChecked(int pos) {
        for (int i = 0; i < mButtons.size(); i++) {
            if (i != pos) mButtons.get(i).setChecked(false);
        }
    }


    /**
     * ViewHolder class for the ButtonsAdapter
     */
    class ButtonViewHolder extends RecyclerView.ViewHolder {
        final Handler handler = new Handler();
        RadioButton mToggleButton;
        Answer mAnswer;
        ButtonViewState mButtonViewState;
        ButtonChoiceListener buttonChoiceListener;

        public ButtonViewHolder(View itemView) {
            super(itemView);
            mToggleButton = (RadioButton) itemView.findViewById(R.id.toggleButton);
//            mSelectionCorrectMarker = (ImageView) itemView.findViewById(R.id.selectionCorrectIndicator);
//            cardView = (FrameLayout) itemView.findViewById(R.id.card_view);
        }

        /**
         * Modifies the button to show correct/incorrect background
         *
         * @param correct Decides which background is shown
         */
        public void adjustBackground(boolean correct) {
            final AnimationDrawable drawable = new AnimationDrawable();

            int colorDefault = context.getResources().getColor(R.color.cardview_light_background);
            int colorRight = context.getResources().getColor(R.color.color_right);
            int colorWrong = context.getResources().getColor(R.color.color_wrong);
            int timeFrame = 400;

            // Correct answers get a green background
            if (correct) {
                drawable.addFrame(new ColorDrawable(colorRight), timeFrame);
            } else {
                if (mToggleButton.isChecked()) {
                    drawable.addFrame(new ColorDrawable(colorWrong), timeFrame);
                } else {
                    drawable.addFrame(new ColorDrawable(colorDefault), timeFrame);
                }
            }
            drawable.addFrame(new ColorDrawable(colorDefault), timeFrame);
            drawable.setOneShot(false);
            mToggleButton.setBackgroundDrawable(drawable);
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    drawable.start();
                }
            }, 100);
        }

        /**
         * Updates the view to indicate whether or not the answer was checked correctly.
         *
         * @return true, if this answer is in the correct pressed state, else false
         */
        public boolean performAnswerCheck() {
            adjustBackground(mAnswer.getAnswerCorrect());
            // Correct selections receive a cross or check
            boolean answerCorrect = mAnswer.getAnswerCorrect() == mToggleButton.isChecked();
            return answerCorrect;
        }

        /**
         * Binds a toggle button to the given view state
         *
         * @param buttonViewState the view state to bind the button to
         */
        public void bind(ButtonViewState buttonViewState) {
            mAnswer = buttonViewState.getAnswer();
            mButtonViewState = buttonViewState;
            mToggleButton.setText(mAnswer.getText());
            mToggleButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    mButtonViewState.setToggleState(mToggleButton.isChecked());
                    if (isChecked) {
                        if (buttonChoiceListener != null) {
                            buttonChoiceListener.onChecked(getAdapterPosition());
                        }
                    }
                }
            });
            mToggleButton.setChecked(buttonViewState.getToggleState());
//            adjustBackground(false); // Reset background
//            mSelectionCorrectMarker.bringToFront();
//            mSelectionCorrectMarker.setVisibility(View.GONE);
        }


        public void setButtonChoiceListener(ButtonChoiceListener buttonChoiceListener) {
            this.buttonChoiceListener = buttonChoiceListener;
        }

        public void setChecked(boolean b) {
            this.mToggleButton.setChecked(b);
        }
    }


}