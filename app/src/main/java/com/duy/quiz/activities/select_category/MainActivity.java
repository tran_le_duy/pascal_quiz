package com.duy.quiz.activities.select_category;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;

import com.duy.quiz.QuizApplication;
import com.duy.quiz.QuizComponent;
import com.duy.quiz.R;
import com.duy.quiz.activities.QuizActivity;
import com.duy.quiz.activities.selectuser.UserSelectionActivity;
import com.duy.quiz.database.DatabaseModule;
import com.duy.quiz.database.UserDataSource;
import com.duy.quiz.logic.UserManager;

/**
 * Created by funkv on 20.02.2016.
 * <p>
 * The activity redirects to user creation on first launch, or loads last selected user if it has
 * been launched before.
 */
public class MainActivity extends QuizActivity implements NavigationView.OnNavigationItemSelectedListener {
    public static String EXTRA_SHOW_LOGGEDIN_SNACKBAR = "SHOW_SNACKBAR";

    @Override
    protected void injectComponent(QuizComponent component) {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);

        // Set as Actionbar
        setSupportActionBar(toolbar);

        NavigationView navigationView = (NavigationView) findViewById(R.id.navigation_view);
        navigationView.setNavigationItemSelectedListener(this);

        DrawerLayout drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar,
                R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawerLayout.setDrawerListener(toggle);
        toggle.syncState();


    }

    /**
     * This method is called hen the activity is started.
     * Shows a snackbar containing the current username when the app is started.
     */
    @Override
    protected void onStart() {
        super.onStart();
        Intent intent = getIntent();
        if (intent == null) {
            return;
        }
        new ShowToastLoginTask().execute(intent);

    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_switch_user:
                startActivity(new Intent(getApplicationContext(), UserSelectionActivity.class));
                return true;
            default:
                return true;
        }
    }

    private class ShowToastLoginTask extends AsyncTask<Intent, Void, Boolean> {

        UserManager mUserManager;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Boolean doInBackground(Intent... params) {
            QuizApplication application = (QuizApplication) getApplication();
            DatabaseModule databaseModule = application.getDatabaseModule();
            UserDataSource userDataSource = new UserDataSource(databaseModule.provideSession());
            mUserManager = new UserManager(application, userDataSource);
            mUserManager.logInLastUser();

            Intent intent = params[0];
            // If EXTRA_SHOW_LOGGEDIN_SNACKBAR is passed,
            // show a little snackbar that shows the currently logged in user's name
            if (intent.getBooleanExtra(EXTRA_SHOW_LOGGEDIN_SNACKBAR, false)) {
                // Update the intent so it doesn't show again on back navigation and thus only when explicitly requested
                intent.putExtra(EXTRA_SHOW_LOGGEDIN_SNACKBAR, false);
                return true;
            }
            return false;
        }

        @Override
        protected void onPostExecute(Boolean aVoid) {
            super.onPostExecute(aVoid);
            View rootView = findViewById(R.id.main_content);
            String text = String.format(getResources().getString(R.string.logged_in_as),
                    mUserManager.getCurrentUser().getName());
            final Snackbar snackbar = Snackbar
                    .make(rootView, text, Snackbar.LENGTH_LONG)
                    .setAction(R.string.switch_user_short, new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Intent intent = new Intent(getApplicationContext(), UserSelectionActivity.class);
                            startActivity(intent);
                        }
                    });
            // Delay the snackbar a quater second for a smoother experience
            rootView.postDelayed(new Runnable() {
                @Override
                public void run() {
                    snackbar.show();
                }
            }, 250);
        }
    }
}