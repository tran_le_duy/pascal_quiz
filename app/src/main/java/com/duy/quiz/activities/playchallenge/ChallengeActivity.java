package com.duy.quiz.activities.playchallenge;


import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Chronometer;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.akexorcist.roundcornerprogressbar.RoundCornerProgressBar;
import com.duy.quiz.QuizApplication;
import com.duy.quiz.QuizComponent;
import com.duy.quiz.R;
import com.duy.quiz.activities.QuizActivity;
import com.duy.quiz.custom_view.ProgressBarAnimation;
import com.duy.quiz.database.CategoryDataSource;
import com.duy.quiz.database.ChallengeDataSource;
import com.duy.quiz.database.DatabaseModule;
import com.duy.quiz.database.StatisticsDataSource;
import com.duy.quiz.database.UserDataSource;
import com.duy.quiz.logic.DueChallengeLogic;
import com.duy.quiz.logic.UserLogicFactory;
import com.duy.quiz.logic.UserManager;
import com.duy.quiz.model.Challenge;
import com.duy.quiz.model.Statistics;
import com.duy.quiz.model.User;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;

/**
 * Created by Christian Kost
 * <p/>
 * Activity used to handle test_pascal. Loads fragments depending on the
 * challenge type. Implements the interface to determine when an challenge was checked and when a
 * self check challenge was checked.
 */
public class ChallengeActivity extends QuizActivity implements AnswerFragment.AnswerListener {
    //Strings used for Bundles
    public static final String EXTRA_CATEGORY_ID = "KEY_CURRENT_CATEGORY_ID";
    public static final String KEY_CHALLENGE_ID = "KEY_CHALLENGE_ID";
    public static final String KEY_NEXT_ON_FAB = "KEY_NEXT_ON_FAB";
    public static final String KEY_ALL_DUE_CHALLENGES = "KEY_ALL_DUE_CHALLENGES";
    private static final String TAG = ChallengeActivity.class.getSimpleName();

    private UserManager mUserManager;
    private StatisticsDataSource mStatisticsDataSource;
    private ChallengeDataSource mChallengeDataSource;
    private CategoryDataSource mCategoryDataSource;
    private UserLogicFactory mUserLogicFactory;
    private AnswerFragmentFactory mAnswerFragmentFactory;

    private ArrayList<Long> mAllDueChallenges;

    //current state stuff
    private int mChallengeNo = 0;
    private Challenge mCurrentChallenge;
    private boolean mLoadNextChallengeOnFabClick = false;
    //View Stuff
    private FloatingActionButton mFloatingActionButton;
    private TextView mQuestionText;

    //    private TextView mCategoryText;
//    private TextView mTypeText;
    private ProgressBar mProgress;
    private int numQuestion = 0; //number question
    private int numRight = 0; //right answer
    private int numWrong = 0; //wrong answer
    private Handler handler = new Handler();
    private Chronometer mChronometer;
    /**
     * {@inheritDoc}
     */
    @Override
    protected void injectComponent(QuizComponent component) {
        QuizApplication application = (QuizApplication) getApplication();
        DatabaseModule databaseModule = application.getDatabaseModule();

        UserDataSource userDataSource = databaseModule.provideUserDataSource();
        mUserManager = new UserManager(application, userDataSource);
        mUserManager.logInLastUser();

        mChallengeDataSource = databaseModule.provideChallengeDataSource();
        mStatisticsDataSource = databaseModule.provideStatisticsDataSource();
        mUserLogicFactory = databaseModule.provideUserLogicFactory();
        mAnswerFragmentFactory = new AnswerFragmentFactory();
        mCategoryDataSource = databaseModule.provideCategoryDataSource();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_challenge);
        setupActionBar();

        mFloatingActionButton = (FloatingActionButton) findViewById(R.id.btnNextChallenge);
        mQuestionText = (TextView) findViewById(R.id.challengeQuestion);
        mProgress = (ProgressBar) findViewById(R.id.progress_bar);
        mChronometer = (Chronometer) findViewById(R.id.chronometer);
        mChronometer.start();
//        mCategoryText = (TextView) findViewById(R.id.challenge_category);
//        mTypeText = (TextView) findViewById(R.id.challenge_type);

        //Get the CategoryID from the intent. If no Category is given the id will be -1 (for all categories)
        Intent i = getIntent();
        long categoryId = i.getLongExtra(EXTRA_CATEGORY_ID, -1);

        //Load the user's due test_pascal for the selected category
        if (savedInstanceState != null) {
            mAllDueChallenges = (ArrayList<Long>) savedInstanceState.getSerializable(KEY_ALL_DUE_CHALLENGES);
            mLoadNextChallengeOnFabClick = savedInstanceState.getBoolean(KEY_NEXT_ON_FAB);
            mChallengeNo = savedInstanceState.getInt(KEY_CHALLENGE_ID, 0);

            //Load the empty state screen if no test_pascal are due
            if (mAllDueChallenges == null || mAllDueChallenges.size() < 1) {
                loadFinishScreen();
                return;
            }

            //load the challenge if no saved instance state, else subviews are responsible for loading state
            mCurrentChallenge = mChallengeDataSource.getById(mAllDueChallenges.get(mChallengeNo));
            initializeMetaData();
            mQuestionText.setText(mCurrentChallenge.getQuestion());
            invalidateProgress();
        } else {
            new LoadQuestions().execute(categoryId);
        }


        // Setup Floating Action Button
        if (mLoadNextChallengeOnFabClick) {
            mFloatingActionButton.setImageResource(R.drawable.ic_navigate_next_white_24dp);
        } else {
            mFloatingActionButton.setImageResource(R.drawable.ic_check_white_24dp);
        }

        mFloatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                floatingActionButtonClicked();
            }
        });
    }

    private void setupActionBar() {
        // Set up actionbar
        Toolbar myChildToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(myChildToolbar);
        ActionBar ab = getSupportActionBar();
        if (ab != null) {
            ab.setDisplayHomeAsUpEnabled(true);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * Loads the current challenge into its fragment depending on the challenge-type
     */
    private void loadQuestion() {
        //Bundle to transfer the ChallengeId to the fragments
        Bundle fragmentArguments = new Bundle();
        fragmentArguments.putLong(KEY_CHALLENGE_ID, mCurrentChallenge.getId());

        //Start a transaction on the fragments
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.disallowAddToBackStack();

        mQuestionText.setText(mCurrentChallenge.getQuestion());

        AnswerFragment fragment = mAnswerFragmentFactory.createFragmentForType(mCurrentChallenge.getChallengeType());
        fragment.setArguments(fragmentArguments);
        transaction.replace(R.id.challenge_fragment, fragment, "" + mCurrentChallenge.getChallengeType());

        //Commit the changes
        transaction.commit();
        getSupportFragmentManager().executePendingTransactions();

        // Load new meta data
        initializeMetaData();
    }

    /**
     * Loads the current challenge's metadata into texts
     */
    private void initializeMetaData() {
        String category = mCategoryDataSource.getById(mCurrentChallenge.getCategoryId()).getTitle();
        setTitle(category);
    }

    /**
     * On Click for mFloatingActionButton
     * <p/>
     * Delegate to Fragment or load the next question
     */
    private void floatingActionButtonClicked() {
        if (mLoadNextChallengeOnFabClick) {
            loadNextScreen();
            mLoadNextChallengeOnFabClick = false;
            return;
        }

        // Find the current fragment
        AnswerFragment currentFragment = (AnswerFragment) getSupportFragmentManager().findFragmentById(R.id.challenge_fragment);

        // Instruct the Fragment to begin answer checking
        AnswerFragment.ContinueMode continueMode = currentFragment.goToNextState();
        switch (continueMode) {
            case CONTINUE_HIDE_FAB:
                mFloatingActionButton.setVisibility(View.GONE);
                break;
            case CONTINUE_SHOW_FAB:
                mLoadNextChallengeOnFabClick = true;
                mFloatingActionButton.setImageResource(R.drawable.ic_navigate_next_white_24dp);
                break;
            case CONTINUE_ABORT:
                break;
        }
    }

    /**
     * After the answer was checked save the answer depending on whether the answer is right or
     * wrong
     *
     * @param answer Answer right or wrong
     */
    @Override
    public void onAnswerChecked(boolean answer, boolean switchToNext) {
        User currentUser = mUserManager.getCurrentUser();
        numQuestion++;
        if (answer)
            numRight++;
        else
            numWrong++;

        // Save the user completion for due calculation
        // TODO: 20-Mar-17  new feature
//        mCompletionLogic.updateAfterAnswer(
//                mAllDueChallenges.get(mChallengeNo),
//                currentUser.getId(),
//                answer ? CompletionLogic.ANSWER_RIGHT : CompletionLogic.ANSWER_WRONG);

        //Create statistics entry
        Statistics statistics = new Statistics(
                null, answer, new Date(),
                currentUser.getId(),
                mAllDueChallenges.get(mChallengeNo));
        mStatisticsDataSource.create(statistics);
        if (switchToNext) {
            loadNextScreen();
        }
    }

    /**
     * Loads the next screen (next challenge or the finish screen)
     */
    private void loadNextScreen() {
        //If the all challenge are completed load the finish screen
        if (mChallengeNo == mAllDueChallenges.size() - 1) {
            loadFinishScreen();
            return;
        }

        //increment counter
        mChallengeNo += 1;
        mCurrentChallenge = mChallengeDataSource.getById(mAllDueChallenges.get(mChallengeNo));

        //load the next challenge
        loadQuestion();
        invalidateProgress();

        mFloatingActionButton.setVisibility(View.VISIBLE);
        mFloatingActionButton.setImageResource(R.drawable.ic_check_white_24dp);
    }


    /**
     * Loads the finish screen and unloads all other screens
     */
    private void loadFinishScreen() {

        // Remove anchor by resetting layout params since anchor element has been removed from the screen
        CoordinatorLayout.LayoutParams lp = new CoordinatorLayout.LayoutParams(CoordinatorLayout.LayoutParams.WRAP_CONTENT, CoordinatorLayout.LayoutParams.WRAP_CONTENT);
        mFloatingActionButton.setLayoutParams(lp);
        mFloatingActionButton.setVisibility(View.INVISIBLE);

        LinearLayout contentLayout = (LinearLayout) findViewById(R.id.challenge_rootcontainer);
        if (contentLayout != null) {
            contentLayout.removeAllViews();
            View view = getLayoutInflater().inflate(R.layout.finish_challenge, contentLayout, false);
            contentLayout.addView(view);
        }
        showListResult();
    }

    private void showListResult() {
        handler.post(new Runnable() {
            @Override
            public void run() {
                RoundCornerProgressBar progressBarAll = (RoundCornerProgressBar) findViewById(R.id.progressBar_all_question);
                RoundCornerProgressBar progressBarRight = (RoundCornerProgressBar) findViewById(R.id.progressBar_right_question);
                RoundCornerProgressBar progressBarWrong = (RoundCornerProgressBar) findViewById(R.id.progressBar_wrong_question);
                TextView txtAll = (TextView) findViewById(R.id.txt_all);
                TextView txtRight = (TextView) findViewById(R.id.txt_right);
                TextView txtWrong = (TextView) findViewById(R.id.txt_wrong);
                progressBarAll.setMax(numQuestion);
                progressBarAll.setProgress(numQuestion);
                progressBarRight.setMax(numQuestion);
                progressBarRight.setProgress(numRight);
                progressBarWrong.setMax(numQuestion);
                progressBarWrong.setProgress(numWrong);
                txtAll.append(" " + numQuestion);
                txtRight.append(" " + numRight);
                txtWrong.append(" " + numWrong);
                ImageView imageView = (ImageView) findViewById(R.id.icon_state);
                float rate = numRight / numQuestion;
                if (rate > 0.7f) {
                    imageView.setImageResource(R.drawable.ic_good);
                } else if (rate > 0.4f) {
                    imageView.setImageResource(R.drawable.ic_normal);
                } else {
                    imageView.setImageResource(R.drawable.ic_bad);
                }
            }
        });
    }

    /**
     * Saves the current state (is answer was checked, current challenge no, due test_pascal)
     *
     * @param savedInstanceState Current state of the due test_pascal, the current challenge and if
     *                           the challenge is done
     */
    @Override
    protected void onSaveInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);
        savedInstanceState.putInt(KEY_CHALLENGE_ID, mChallengeNo);
        savedInstanceState.putBoolean(KEY_NEXT_ON_FAB, mLoadNextChallengeOnFabClick);
        savedInstanceState.putSerializable(KEY_ALL_DUE_CHALLENGES, mAllDueChallenges);
    }

    private void invalidateProgress() {
        ProgressBarAnimation anim = new ProgressBarAnimation(mProgress, mProgress.getProgress(), (mChallengeNo + 1) * 100);
        anim.setDuration(500);
        mProgress.startAnimation(anim);
    }

    /**
     * load question in database in thread, it can progress greater than one second.
     */
    private class LoadQuestions extends AsyncTask<Long, Void, Void> {
        private ProgressDialog dialog;

        /**
         * show dialog, disable cancel
         */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog = new ProgressDialog(ChallengeActivity.this);
            dialog.setCancelable(false);
            dialog.setCanceledOnTouchOutside(false);
            dialog.setMessage(getString(R.string.loading_data));
            dialog.show();
        }

        @Override
        protected void onCancelled() {
            super.onCancelled();
            dialog.cancel();
        }

        @Override
        protected void onCancelled(Void aVoid) {
            super.onCancelled(aVoid);
            dialog.cancel();
        }

        /**
         * Load question
         */
        @Override
        protected Void doInBackground(Long... params) {
            final User currentUser = mUserManager.getCurrentUser();
            DueChallengeLogic dueChallengeLogic = mUserLogicFactory.createDueChallengeLogic(currentUser);
            mAllDueChallenges = new ArrayList<>(dueChallengeLogic.getDueChallenges(params[0]));
            Collections.shuffle(mAllDueChallenges);
            return null;
        }

        /**
         * if count of question is zero, load finish screen, else load question.
         * <p>
         * dismiss dialog
         */
        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            //Load the empty state screen if no test_pascal are due
            if (mAllDueChallenges == null || mAllDueChallenges.size() < 1) {
                loadFinishScreen();
                return;
            }

            //load the challenge if no saved instance state, else subviews are responsible for loading state
            mCurrentChallenge = mChallengeDataSource.getById(mAllDueChallenges.get(mChallengeNo));

            loadQuestion();

            //setup progressbar
            mProgress.setMax(mAllDueChallenges.size() * 100);
            invalidateProgress();
            dialog.cancel();
        }


    }
}