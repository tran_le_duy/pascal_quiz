package com.duy.quiz.activities.playchallenge.multiplechoice;

/**
 * Created by Duy on 19-Mar-17.
 */

public interface ButtonChoiceListener {
    void onChecked(int pos);
}
