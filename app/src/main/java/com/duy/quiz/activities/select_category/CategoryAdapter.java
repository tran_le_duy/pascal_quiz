package com.duy.quiz.activities.select_category;


import android.content.Context;
import android.support.v4.util.LongSparseArray;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.duy.quiz.R;
import com.duy.quiz.database.CategoryDataSource;
import com.duy.quiz.model.Category;

import java.util.List;

public class CategoryAdapter extends RecyclerView.Adapter<CategoryViewHolder> {
    private List<Category> mCategories;
    private SelectionListener mListener;
//    private LongSparseArray<Integer> mDueChallengeCounts = new LongSparseArray<>();
    private LayoutInflater inflater;

    /**
     * Adapter for Listing Categories in a recycler view
     *
     * @param activity
     * @param categories         list of categories to show
     * @param listener           listener that is notified when a category is clicked.
     */
    public CategoryAdapter(Context activity, List<Category> categories,
                           SelectionListener listener) {
        mListener = listener;
        mCategories = categories;
//        mDueChallengeCounts = dueChallengeCounts;
        setHasStableIds(true);
        inflater = LayoutInflater.from(activity);
    }

    /**
     * Called to create the ViewHolder at the given position.
     *
     * @param parent   parent to assign the newly created view to
     * @param viewType ignored
     */
    @Override
    public CategoryViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = inflater.inflate(R.layout.list_item_category, parent, false);
        return new CategoryViewHolder(v, mListener);
    }

    /**
     * Called to bind the ViewHolder at the given position.
     *
     * @param holder   the ViewHolder object to be bound
     * @param position the position of the ViewHolder
     */
    @Override
    public void onBindViewHolder(CategoryViewHolder holder, int position) {
        final Category category = mCategories.get(position);
        holder.bindCard(category/*, mDueChallengeCounts.get(category.getId())*/);
    }

    /**
     * Returns the count of ViewHolders in the adapter
     *
     * @return the count of ViewHolders
     */
    @Override
    public int getItemCount() {
        return mCategories.size();
    }

    /**
     * Returns the stable ID for the item at <code>position</code>.
     *
     * @param position Adapter position to query
     * @return the stable ID of the item at position
     */
    @Override
    public long getItemId(int position) {
        if (position == 0) {
            return CategoryDataSource.CATEGORY_ID_ALL;
        }
        Category category = mCategories.get(position - 1);
        return category.getId();
    }

    /**
     * Interface to pass selection events.
     */
    public interface SelectionListener {
        void onCategorySelected(Category category);

        void onAllCategoriesSelected();

        void onCategoryStatisticsSelected(Category category);

        void onAllCategoriesStatisticsSelected();
    }
}
