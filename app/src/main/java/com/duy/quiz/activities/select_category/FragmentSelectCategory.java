package com.duy.quiz.activities.select_category;


import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.duy.quiz.QuizApplication;
import com.duy.quiz.QuizComponent;
import com.duy.quiz.R;
import com.duy.quiz.activities.playchallenge.ChallengeActivity;
import com.duy.quiz.activities.statistics.StatisticsActivity;
import com.duy.quiz.database.CategoryDataSource;
import com.duy.quiz.database.DatabaseModule;
import com.duy.quiz.fragments.QuizFragment;
import com.duy.quiz.model.Category;

import java.util.List;

/**
 * Created by funkv on 17.02.2016.
 */
public class FragmentSelectCategory extends QuizFragment implements CategoryAdapter.SelectionListener {
    private static final String TAG = FragmentSelectCategory.class.getSimpleName();
    private List<Category> mCategories;
//    private LongSparseArray<Integer> mDueChallengeCounts = new LongSparseArray<>();
    private RecyclerView mRecyclerView;

    @Override
    protected void injectComponent(QuizComponent component) {

    }

    /**
     * This method is called when the fragment is created
     *
     * @param savedInstanceState handed over to super constructor
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        QuizApplication application = (QuizApplication) getActivity().getApplication();
        DatabaseModule databaseModule = application.getDatabaseModule();
//        UserDataSource userDataSource = databaseModule.provideUserDataSource();
//        UserManager mUserManager = new UserManager(application, userDataSource);
//        mUserManager.logInLastUser();

//        UserLogicFactory mUserLogicFactory = new UserLogicFactory(application, databaseModule.provideSession());
//        DueChallengeLogic mDueChallengeLogic = mUserLogicFactory.createDueChallengeLogic(mUserManager.getCurrentUser());
//        mDueChallengeCounts = mDueChallengeLogic.getDueChallengeCounts(mCategories);

        CategoryDataSource mCategoryDataSource = databaseModule.provideCategoryDataSource();
        mCategories = mCategoryDataSource.getAll();
    }

    /**
     * This method is called to create th fragment view
     *
     * @param inflater           the layout inflater to inflate the layout with
     * @param container          the container the view is created in
     * @param savedInstanceState ignored
     * @return the created view
     */
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_select_category, container, false);
        mRecyclerView = (RecyclerView) rootView.findViewById(R.id.recyclerView);
        return rootView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(layoutManager);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setAdapter(new CategoryAdapter(getActivity(), mCategories, FragmentSelectCategory.this));
    }

    /**
     * This method is called when a category was selected
     *
     * @param category the category that has been selected
     */
    @Override
    public void onCategorySelected(Category category) {
        Intent intent = new Intent(getContext(), ChallengeActivity.class);
        intent.putExtra(ChallengeActivity.EXTRA_CATEGORY_ID, category.getId());
        startActivity(intent);
    }

    /**
     * This method is called when all categories were selected
     */
    @Override
    public void onAllCategoriesSelected() {
        Intent intent = new Intent(getContext(), ChallengeActivity.class);
        intent.putExtra(ChallengeActivity.EXTRA_CATEGORY_ID, CategoryDataSource.CATEGORY_ID_ALL);
        startActivity(intent);
    }

    /**
     * This method is called when the statistic button of a category was clicked
     *
     * @param category the category that has been selected
     */
    @Override
    public void onCategoryStatisticsSelected(Category category) {
        Intent intent = new Intent(getActivity().getApplicationContext(), StatisticsActivity.class);
        intent.putExtra(ChallengeActivity.EXTRA_CATEGORY_ID, category.getId());
        startActivity(intent);
    }

    /**
     * This method is called when the statistic button of all categories was clicked
     */
    @Override
    public void onAllCategoriesStatisticsSelected() {
        Intent intent = new Intent(getActivity().getApplicationContext(), StatisticsActivity.class);
        intent.putExtra(ChallengeActivity.EXTRA_CATEGORY_ID, CategoryDataSource.CATEGORY_ID_ALL);
        startActivity(intent);
    }


}
