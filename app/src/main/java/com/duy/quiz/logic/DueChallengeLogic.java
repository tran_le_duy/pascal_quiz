package com.duy.quiz.logic;

import android.support.v4.util.LongSparseArray;

import com.duy.quiz.database.CategoryDataSource;
import com.duy.quiz.database.ChallengeDataSource;
import com.duy.quiz.database.CompletionDataSource;
import com.duy.quiz.model.Category;
import com.duy.quiz.model.Challenge;
import com.duy.quiz.model.Completion;
import com.duy.quiz.model.User;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Daniel Hoogen on 25/02/2016.
 * <p/>
 * This class contains the logic for reading due test_pascal from the databases
 */
public class DueChallengeLogic {
    //Attributes
    private User mUser;

    private CompletionDataSource mCompletionDataSource;
    private ChallengeDataSource mChallengeDataSource;

    /**
     * Constructor which saves the given parameters as member attributes.
     *
     * @param user                 the user to be saved as a member attribute
     * @param completionDataSource the completion data source to be saved as a member attribute
     * @param challengeDataSource  the completion data source to be saved as a member attribute
     */
    public DueChallengeLogic(User user, CompletionDataSource completionDataSource,
                             ChallengeDataSource challengeDataSource) {
        mCompletionDataSource = completionDataSource;
        mChallengeDataSource = challengeDataSource;
        mUser = user;
    }

    /**
     * Returns the amount of due test_pascal for each category id.
     *
     * @param categories the list of categories
     * @return array that maps category counts to category ids. (Key = CategoryId, Value = Count)
     */
    public LongSparseArray<Integer> getDueChallengeCounts(List<Category> categories) {
        LongSparseArray<Integer> dueChallengeCounts = new LongSparseArray<>();
        for (Category category : categories) {
            int challengesDueCount = getDueChallenges(category.getId()).size();
            dueChallengeCounts.put(category.getId(), challengesDueCount);
        }
        return dueChallengeCounts;
    }

    /**
     * Returns a list with the ids of all due test_pascal of the given user in the category with the
     * given id. If the given category id is CategoryDataSource.CATEGORY_ID_ALL, the due test_pascal
     * of all categories will be returned. Challenges without entries in the completed table will
     * be returned and the missing entries will be created.
     *
     * @param categoryId the id of the category whose due test_pascal will be returned
     * @return List object containing the ids of all due test_pascal
     */
    public List<Long> getDueChallenges(long categoryId) {
        //Create list that will be returned in the end
        List<Long> dueChallenges = new ArrayList<>();

        long time = System.currentTimeMillis();
        //Get due test_pascal that have entries in the database
        addQuestionsByCategory(dueChallenges, categoryId);
        System.out.println("tAime " + (System.currentTimeMillis() - time));
        //Create missing entries
//        createMissingCompletionsByCategory(dueChallenges, categoryId);

        //Return list
        return dueChallenges;
    }

    /**
     * Adds the ids of all due test_pascal of the given user in the category with the given id to
     * the given list. If the given category id is CategoryDataSource.CATEGORY_ID_ALL, the due
     * test_pascal of all categories will be added.
     *
     * @param dueChallenges the list object the due test_pascal will be added to
     * @param categoryId    the id of the category whose due test_pascal will be returned
     */
    private void addQuestionsByCategory(List<Long> dueChallenges, long categoryId) {
        //Check for each stage if their test_pascal are due
        for (int stage = 1; stage <= 6; stage++) {
            //Get the users completions in the stage
            List<Completion> completedInStage;
            completedInStage = mCompletionDataSource.findByUserAndStage(mUser, stage);
            //Check for all test_pascal if they are due and in the correct category
            for (Completion completed : completedInStage) {
                if ((categoryId == CategoryDataSource.CATEGORY_ID_ALL
                        || categoryId == completed.getChallenge().getCategoryId())) {
                    dueChallenges.add(completed.getChallengeId());
                }
            }
        }
    }

    /**
     * Returns all test_pascal that have never been completed by the given user before
     *
     * @return list of uncompleted test_pascal
     */
    public List<Challenge> getUncompletedChallenges() {
        List<Challenge> notCompleted = new ArrayList<>();

        List<Challenge> challenges = mChallengeDataSource.getAll();

        for (Challenge challenge : challenges) {
            if (mCompletionDataSource.findByChallengeAndUser(challenge.getId(), mUser.getId()) == null) {
                notCompleted.add(challenge);
            }
        }

        return notCompleted;
    }

    /**
     * Adds the ids of all due test_pascal without entries in the completed table to the given list.
     * The missing entries will also be created in the completed table.
     *
     * @param dueChallenges the list object the due test_pascal will be added to
     * @param categoryId    the id of the category whose due test_pascal will be added
     */
    private void createMissingCompletionsByCategory(List<Long> dueChallenges, long categoryId) {
        //Get uncompleted test_pascal
        List<Challenge> notCompletedYet = getUncompletedChallenges();

        /*
         * If categoryId is CategoryDataSource.CATEGORY_ID_ALL or matches the challenge's id, the
         * challenge will be added to the list and an entry will be created.
         */
        for (Challenge challenge : notCompletedYet) {
            if (categoryId == CategoryDataSource.CATEGORY_ID_ALL
                    || challenge.getCategoryId() == categoryId) {
                long userId = mUser.getId();
                long challengeId = challenge.getId();

                Completion completed;
                completed = new Completion(null, 1, new Date(0L), userId, challengeId);
                mCompletionDataSource.create(completed);
                dueChallenges.add(challenge.getId());
            }
        }
    }
}