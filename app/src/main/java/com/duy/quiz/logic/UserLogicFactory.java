package com.duy.quiz.logic;

import com.duy.quiz.QuizApplication;
import com.duy.quiz.database.ChallengeDataSource;
import com.duy.quiz.database.CompletionDataSource;
import com.duy.quiz.database.StatisticsDataSource;
import com.duy.quiz.logic.statistics.ChartDataLogic;
import com.duy.quiz.logic.statistics.ChartSettings;
import com.duy.quiz.logic.statistics.StatisticsLogic;
import com.duy.quiz.model.DaoSession;
import com.duy.quiz.model.User;

/**
 * Created by funkv on 06.03.2016.
 * <p/>
 * Factory that is used to create logic objects which require a user.
 * Dependencies are injected automatically.
 */
public class UserLogicFactory {
    private QuizApplication mApplication;
    private CompletionDataSource mCompletionDataSource;
    private ChallengeDataSource mChallengeDataSource;
    private StatisticsDataSource mStatisticsDataSource;
    private ChartSettings mSettings;

    public UserLogicFactory(QuizApplication mApplication, DaoSession daoSession) {
        this.mApplication = mApplication;
        this.mCompletionDataSource = new CompletionDataSource(daoSession);
        this.mChallengeDataSource = new ChallengeDataSource(daoSession);
        this.mStatisticsDataSource = new StatisticsDataSource(daoSession);
        this.mSettings = new ChartSettings(mApplication);
    }

    /**
     * Create a DueChallengeLogic for the specified user.
     *
     * @param user user whose challenges are inspected
     * @return the DueChallengeLogic object
     */
    public DueChallengeLogic createDueChallengeLogic(User user) {
        return new DueChallengeLogic(user, mCompletionDataSource, mChallengeDataSource);
    }

    /**
     * Creates ChartDataLogic
     *
     * @param user
     * @param categoryId category to inspect
     * @return ChartDataLogic
     */
    public ChartDataLogic createChartDataLogic(User user, long categoryId) {
        return new ChartDataLogic(user,
                categoryId,
                mApplication,
                mChallengeDataSource,
                mCompletionDataSource,
                mStatisticsDataSource,
                this);
    }

    /**
     * Create a DueChallengeLogic for the specified user.
     *
     * @param user       user whose challenges are inspected
     * @param categoryId category to inspect
     * @return the DueChallengeLogic object
     */
    public StatisticsLogic createStatisticsLogic(User user, long categoryId) {
        return new StatisticsLogic(mApplication, mSettings, createChartDataLogic(user, categoryId));
    }
}
