package com.duy.quiz.logic.statistics;

/**
 * Created by Daniel Hoogen on 12/03/2016.
 * <p/>
 * Enumeration for the different statistic types
 */
public enum StatisticType {
    TYPE_MOST_PLAYED,
    TYPE_MOST_FAILED,
    TYPE_HISTORY,
    TYPE_MOST_SUCCEEDED,
    TYPE_ALL_QUESTION
}