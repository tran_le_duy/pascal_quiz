package com.duy.quiz;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;

import com.duy.quiz.database.AnswerDataSource;
import com.duy.quiz.database.CategoryDataSource;
import com.duy.quiz.database.ChallengeDataSource;
import com.duy.quiz.database.CompletionDataSource;
import com.duy.quiz.database.DatabaseModule;
import com.duy.quiz.database.StatisticsDataSource;
import com.duy.quiz.model.Answer;
import com.duy.quiz.model.DaoSession;

import java.util.List;

public class TestActivity extends AppCompatActivity {

    private static final String TAG = TestActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        QuizApplication application = (QuizApplication) getApplication();
        DatabaseModule databaseModule = application.getDatabaseModule();
        DaoSession daoSession = databaseModule.provideSession();
        AnswerDataSource answerDataSource = new AnswerDataSource(daoSession);
        CategoryDataSource categoryDataSource = new CategoryDataSource(daoSession);
        ChallengeDataSource challengeDataSource = new ChallengeDataSource(daoSession);
        CompletionDataSource completionDataSource = new CompletionDataSource(daoSession);
        StatisticsDataSource statisticsDataSource = new StatisticsDataSource(daoSession);

        List<Answer> answerList = answerDataSource.getAll();
        for (Answer answer : answerList) {
            Log.d(TAG, "onCreate: " + answer.toString());
        }
    }

}
